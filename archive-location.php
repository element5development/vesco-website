<?php 
/*----------------------------------------------------------------*\

	DEFAULT ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
?>


<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php get_template_part('template-parts/sections/headers/header-archives'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>
	<article> 
		<section class="stats">
			<?php while ( have_rows('stats', 'option') ) : the_row(); ?>
				<div>
					<p><span><?php the_sub_field('stat'); ?></span><br/><?php the_sub_field('description'); ?></p>
				</div>
			<?php endwhile; ?> 
		</section>
		<section class="state michigan">
			<div class="introduction">
				<?php $image = get_field('michigan_image', 'option'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
				<h3>Michigan</h3>
				<?php the_field('michigan_description', 'option'); ?>
			</div>
			<?php
				$args = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'location',
					'meta_key'		=> 'state',
					'meta_value'	=> 'MI'
				);
				$the_query = new WP_Query( $args );
			?>
			<div class="locations">
				<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php get_template_part('template-parts/elements/previews/preview-' . $posttype); ?>
				<?php endwhile; ?>
			</div>
		</section>
		<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
		<hr>
		<section class="state ohio">
			<div class="introduction">
				<?php $image = get_field('ohio_image', 'option'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
				<h3>Ohio</h3>
				<?php the_field('ohio_description', 'option'); ?>
			</div>
			<?php
				$args = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'location',
					'meta_key'		=> 'state',
					'meta_value'	=> 'OH'
				);
				$the_query = new WP_Query( $args );
			?>
			<div class="locations">
				<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php get_template_part('template-parts/elements/previews/preview-' . $posttype); ?>
				<?php endwhile; ?>
			</div>
		</section>
		<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
		<hr>
		<section class="state pennsylvania">
			<div class="introduction">
				<?php $image = get_field('pennsylvania_image', 'option'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
				<h3>Pennsylvania</h3>
				<?php the_field('pennsylvania_description', 'option'); ?>
			</div>
			<?php
				$args = array(
					'posts_per_page'	=> -1,
					'post_type'			=> 'location',
					'meta_key'		=> 'state',
					'meta_value'	=> 'PA'
				);
				$the_query = new WP_Query( $args );
			?>
			<div class="locations">
				<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<?php get_template_part('template-parts/elements/previews/preview-' . $posttype); ?>
				<?php endwhile; ?>
			</div>
		</section>
		<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>