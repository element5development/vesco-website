<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	Standard page template for website which should include all the
	commonly used options.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php	get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<?php 
	if ( get_field('sidebar') == 'both') :
		$asideClass = 'aside-both';
	elseif ( get_field('sidebar') == 'right') :
		$asideClass = 'aside-right';
	elseif ( get_field('sidebar') == 'left') :
		$asideClass = 'aside-left';
	else :
		$asideClass = '';
	endif;
?>

<main class="<?php echo $asideClass; ?>">
	<article class="<?php echo $asideClass; ?>">
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'editor_with_fixed_image' ): 
						get_template_part('template-parts/sections/fixed-image');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'icon_grid' ): 
						get_template_part('template-parts/sections/icons');
					elseif( get_row_layout() == 'standard_cards' ): 
						get_template_part('template-parts/sections/standard-cards');
					elseif( get_row_layout() == 'product_cards' ): 
						get_template_part('template-parts/sections/product-cards');
					elseif( get_row_layout() == 'flip_cards' ): 
						get_template_part('template-parts/sections/flip-cards');
					elseif( get_row_layout() == 'stats' ): 
						get_template_part('template-parts/sections/stats');
					endif;

				endwhile;
			endif; 
		?>
		<?php if ( is_page(295) ) : //CAREER ?>
			<section class="careers">
				<h2>AVAILABLE POSITIONS</h2>
				<script type="text/javascript" src="//jobs.ourcareerpages.com/Resources/js/ccp_widget_support.js" > </script>
				<link rel="stylesheet" href="//jobs.ourcareerpages.com/Resources/css/ccp_widget_support.css" />
				<script type="text/javascript" >
					var options = {
					CCPCode: "VescoOilCorporation"
					,ElementID: "BDHRJobListings"
					,ShowCustomContent: false
					};
					bdhr.generateListing(options);
				</script>
				<div id="BDHRJobListings" > </div>
			</section>
			<section class="wysiwyg-block">
				<p>Vesco Oil is an Equal Opportunity Employer and does not discriminate because of age, color, disability, ethnicity, marital or family status, national origin, race, religion, sex, orientation, military veteran status, or any other characteristic protected by law.</p>
			</section>
		<?php endif; ?>
	</article>
	<?php 
		if ( get_field('sidebar') == 'left' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-left');
		endif;
		if ( get_field('sidebar') == 'right' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-right');
		endif;
	?>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>