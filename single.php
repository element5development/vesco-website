<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php get_template_part('template-parts/sections/headers/header-post'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article> 

		<?php if ( get_field('offsite_article_url') ) : ?>
			<section class="wysiwyg-block">
				<?php the_excerpt(); ?>
				<a class="button is-red" target="_blank" href="<?php the_field('offsite_article_url'); ?>">Continue Reading</a>
			</section>
		<?php else: ?>
			<?php
				if( have_rows('article') ):
					while ( have_rows('article') ) : the_row();

						if( get_row_layout() == 'basic_editor' ):
							get_template_part('template-parts/sections/wysiwyg');
						elseif( get_row_layout() == 'two_col_editor' ): 
							get_template_part('template-parts/sections/wysiwyg-two');
						elseif( get_row_layout() == 'editor_with_fixed_image' ): 
							get_template_part('template-parts/sections/fixed-image');
						elseif( get_row_layout() == 'banner' ): 
							get_template_part('template-parts/sections/banner');
						elseif( get_row_layout() == 'gallery' ): 
							get_template_part('template-parts/sections/gallery');
						elseif( get_row_layout() == 'icon_grid' ): 
							get_template_part('template-parts/sections/icons');
						elseif( get_row_layout() == 'standard_cards' ): 
							get_template_part('template-parts/sections/standard-cards');
						elseif( get_row_layout() == 'product_cards' ): 
							get_template_part('template-parts/sections/product-cards');
						elseif( get_row_layout() == 'flip_cards' ): 
							get_template_part('template-parts/sections/flip-cards');
						elseif( get_row_layout() == 'stats' ): 
							get_template_part('template-parts/sections/stats');
						endif;

					endwhile;
				endif; 
			?>
		<?php endif; ?>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>