var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
			GALLERIES
	\*----------------------------------------------------------------*/
	$('.gallery-items .gallery-item').featherlightGallery({
		previousIcon: '🡠',
		nextIcon: '🡢',
		galleryFadeIn: 100,
		galleryFadeOut: 300
	});
});