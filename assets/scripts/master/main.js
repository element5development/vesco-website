var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
  	SEARCH DIALOG
	\*----------------------------------------------------------------*/
	$('#open-search').click(function () {
		$('.search-dialog').addClass('is-opened');
	});
	$('#close-search').click(function () {
		$('.search-dialog').removeClass('is-opened');
	});
	/*----------------------------------------------------------------*\
		MOBILE NAV
	\*----------------------------------------------------------------*/
	$('.mobile-navigation-block button').click(function () {
		$('nav.mobile-nav').addClass('is-open');
	});
	$('html').click(function (event) {
		if ($(event.target).closest('.mobile-nav, .mobile-navigation-block button').length === 0) {
			$('.mobile-nav').removeClass("is-open");
		}
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
		PRODUCT CARDS
	\*----------------------------------------------------------------*/
	$('.product .front button').click(function () {
		$(this).parents('.product').addClass('is-flipped');
	});
	$('.product .back button').click(function () {
		$(this).parents('.product').removeClass('is-flipped');
	});
	/*----------------------------------------------------------------*\
		FLIP CARDS
	\*----------------------------------------------------------------*/
	$('.flip-cards .card .front.flipper').click(function () {
		$(this).parents('.card').addClass('is-flipped');
	});
	$('.flip-cards .card .back button').click(function () {
		$(this).parents('.card').removeClass('is-flipped');
	});
	/*----------------------------------------------------------------*\
		HISTORY TIMELINE
	\*----------------------------------------------------------------*/
	$('.timeline-events').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		fade: true,
		asNavFor: '.timeline-dates'
	});
	$('.timeline-dates').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.timeline-events',
		dots: false,
		prevArrow: '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M28.75 40a2.43 2.43 0 0 1-1.75-.74L9.49 21.76a2.46 2.46 0 0 1 0-3.52L27 .74a2.46 2.46 0 0 1 3.52 0 2.43 2.43 0 0 1 .74 1.76v35a2.53 2.53 0 0 1-2.5 2.5z"/></svg>',
		nextArrow: '<svg viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.4142"><path d="M11.2588-.0013c.659-.001 1.291.266 1.75.74l17.51 17.5a2.461 2.461 0 0 1 0 3.52l-17.51 17.5a2.461 2.461 0 0 1-3.52 0 2.4308 2.4308 0 0 1-.74-1.76v-35c.016-1.365 1.135-2.484 2.5-2.5h.01z" fill-rule="nonzero"/></svg>',
		focusOnSelect: true,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 2,
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		TITLE SLIDER
	\*----------------------------------------------------------------*/
	$('.page-title.is-slider').slick({
		autoplay: true,
		autoplaySpeed: 3500,
		arrows: false,
		dots: false,
		fade: true,
	});
	/*----------------------------------------------------------------*\
		PRODUCT CARDS
	\*----------------------------------------------------------------*/
	$('.navigation-block nav .menu-about').click(function () {
		$(this).toggleClass('is-active');
		$('.menu-product-services').removeClass('is-active');
		$('.products-dialog').removeClass('is-active');
		$('.about-dialog').toggleClass('is-active');
	});
	$('.navigation-block nav .menu-products-services').click(function () {
		$(this).toggleClass('is-active');
		$('.menu-about').removeClass('is-active');
		$('.about-dialog').removeClass('is-active');
		$('.products-dialog').toggleClass('is-active');
	});

});