<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php 
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<main>
	<article>
		<section class="wysiwyg-block">
			<h1><?php the_field('page_title'); ?></h1>
			<p><?php the_field('title_description'); ?></p>
			<div class="buttons">
				<a class="button is-red" href="/">Return to the Homepage</a>
				<a class="button is-ghost" href="/">Contact Support</a>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>