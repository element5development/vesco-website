<?php 
/*----------------------------------------------------------------*\

	Template Name: History Timeline
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php	get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<?php 
	if ( get_field('sidebar') == 'both') :
		$asideClass = 'aside-both';
	elseif ( get_field('sidebar') == 'right') :
		$asideClass = 'aside-right';
	elseif ( get_field('sidebar') == 'left') :
		$asideClass = 'aside-left';
	else :
		$asideClass = '';
	endif;
?>

<main class="<?php echo $asideClass; ?>">
	<article class="<?php echo $asideClass; ?>">
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'editor_with_fixed_image' ): 
						get_template_part('template-parts/sections/fixed-image');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'icon_grid' ): 
						get_template_part('template-parts/sections/icons');
					elseif( get_row_layout() == 'standard_cards' ): 
						get_template_part('template-parts/sections/standard-cards');
					elseif( get_row_layout() == 'product_cards' ): 
						get_template_part('template-parts/sections/product-cards');
					elseif( get_row_layout() == 'flip_cards' ): 
						get_template_part('template-parts/sections/flip-cards');
					elseif( get_row_layout() == 'stats' ): 
						get_template_part('template-parts/sections/stats');
					endif;

				endwhile;
			endif; 
		?>

		<section class="timeline">
			<div class="timeline-dates">
				<?php while ( have_rows('timeline') ) : the_row(); ?>
					<button class="date"><?php the_sub_field('year'); ?></button>
				<?php endwhile; ?>
			</div>
			<div class="timeline-events">
				<?php while ( have_rows('timeline') ) : the_row(); ?>
					<div class="event">
						<div>
							<div>
								<?php if ( get_sub_field('image') ) : ?>
									<?php $image = get_sub_field('image'); ?>
									<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php else : ?>
									<p><?php the_sub_field('year'); ?></p>
								<?php endif; ?>
							</div>
							<div>
								<h4><?php the_sub_field('year'); ?> - <?php the_sub_field('title'); ?></h4>
								<p><?php the_sub_field('description'); ?></p>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</section>

	</article>
	<?php 
		if ( get_field('sidebar') == 'left' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-left');
		endif;
		if ( get_field('sidebar') == 'right' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-right');
		endif;
	?>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>