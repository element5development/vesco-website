<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php	get_template_part('template-parts/sections/headers/header-front'); ?>

<?php 
	if ( get_field('sidebar') == 'both') :
		$asideClass = 'aside-both';
	elseif ( get_field('sidebar') == 'right') :
		$asideClass = 'aside-right';
	elseif ( get_field('sidebar') == 'left') :
		$asideClass = 'aside-left';
	else :
		$asideClass = '';
	endif;
?>

<main class="<?php echo $asideClass; ?>">
	<article class="<?php echo $asideClass; ?>">
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'editor_with_fixed_image' ): 
						get_template_part('template-parts/sections/fixed-image');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'icon_grid' ): 
						get_template_part('template-parts/sections/icons');
					elseif( get_row_layout() == 'standard_cards' ): 
						get_template_part('template-parts/sections/standard-cards');
					elseif( get_row_layout() == 'product_cards' ): 
						get_template_part('template-parts/sections/product-cards');
					elseif( get_row_layout() == 'flip_cards' ): 
						get_template_part('template-parts/sections/flip-cards');
					elseif( get_row_layout() == 'stats' ): 
						get_template_part('template-parts/sections/stats');
					endif;

				endwhile;
			endif; 
		?>
	</article>
	<?php 
		if ( get_field('sidebar') == 'left' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-left');
		endif;
		if ( get_field('sidebar') == 'right' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-right');
		endif;
	?>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>