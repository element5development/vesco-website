<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php
	get_template_part('template-parts/elements/navigation-mobile'); 
	get_template_part('template-parts/elements/navigation'); 
?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article> 
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'editor_with_fixed_image' ): 
						get_template_part('template-parts/sections/fixed-image');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'icon_grid' ): 
						get_template_part('template-parts/sections/icons');
					elseif( get_row_layout() == 'standard_cards' ): 
						get_template_part('template-parts/sections/standard-cards');
					elseif( get_row_layout() == 'product_cards' ): 
						get_template_part('template-parts/sections/product-cards');
					elseif( get_row_layout() == 'flip_cards' ): 
						get_template_part('template-parts/sections/flip-cards');
					elseif( get_row_layout() == 'stats' ): 
						get_template_part('template-parts/sections/stats');
					endif;

				endwhile;
			endif; 
		?>
		<section class="location-map">
			<div>
				<h4><?php the_field('city'); ?> Warehouse</h4>
				<p><?php the_field('address'); ?><br/><?php the_field('city'); ?> <?php the_field('state'); ?>, <?php the_field('zip'); ?></p>
				<?php 
					$phone = preg_replace("/[^0-9]/", "", get_field('phone_number') );
				?>
				<p><a href="tel:+1<?php echo $phone; ?>"><?php the_field('phone_number'); ?></a></p>
				<?php if ( get_field('fax') ) : ?>
					<p>Fax: <?php the_field('fax'); ?></p>
				<?php endif; ?>
				<a class="button is-red" target="_blank" href="https://maps.google.com/?q=<?php the_field('address'); ?>, <?php the_field('city'); ?>, <?php the_field('state'); ?>, <?php the_field('zip'); ?>">Get Directions</a>
			</div>
			<div>
				<?php the_field('iframe'); ?>
			</div>
		</section>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>