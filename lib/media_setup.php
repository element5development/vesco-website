<?php

/*----------------------------------------------------------------*\
	ADDITIONAL IMAGE SIZES
\*----------------------------------------------------------------*/
add_image_size( 'small', 400, 400 ); 
function small_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'small_image_sizes');
add_image_size( 'xlarge', 2000, 1600 ); 
function large_image_sizes($sizes) {
	$sizes['xlarge'] = __( 'X-Large' );
	return $sizes;
}
add_filter('image_size_names_choose', 'large_image_sizes');
add_image_size( 'gallery', 470, 245, true ); 
function gallery_image_sizes($sizes) {
	$sizes['gallery'] = __( 'Gallery' );
	return $sizes;
}
add_filter('image_size_names_choose', 'gallery_image_sizes');

/*----------------------------------------------------------------*\
	ADDITIONAL FILE FORMATS
\*----------------------------------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['zip'] = 'application/zip';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*----------------------------------------------------------------*\
	ENABLE TAGS FOR MEDIA LIBRARY
\*----------------------------------------------------------------*/
function wptp_add_tags_to_attachments() {
	register_taxonomy_for_object_type( 'post_tag', 'attachment' );
}
add_action( 'init' , 'wptp_add_tags_to_attachments' );