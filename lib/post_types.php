<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Post Type Key: location
function create_location_cpt() {
	$labels = array(
		'name' => __( 'Locations', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Location', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Locations', 'textdomain' ),
		'name_admin_bar' => __( 'Location', 'textdomain' ),
		'archives' => __( 'Location Archives', 'textdomain' ),
		'attributes' => __( 'Location Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Location:', 'textdomain' ),
		'all_items' => __( 'All Locations', 'textdomain' ),
		'add_new_item' => __( 'Add New Location', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Location', 'textdomain' ),
		'edit_item' => __( 'Edit Location', 'textdomain' ),
		'update_item' => __( 'Update Location', 'textdomain' ),
		'view_item' => __( 'View Location', 'textdomain' ),
		'view_items' => __( 'View Locations', 'textdomain' ),
		'search_items' => __( 'Search Location', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Location', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Location', 'textdomain' ),
		'items_list' => __( 'Locations list', 'textdomain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Locations list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Location', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title', 'page-attributes', 'excerpt' ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'location', $args );
}
add_action( 'init', 'create_location_cpt', 0 );

// Post Type Key: resource
function create_resource_cpt() {
	$labels = array(
		'name' => __( 'Resources', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Resource', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Resources', 'textdomain' ),
		'name_admin_bar' => __( 'Resource', 'textdomain' ),
		'archives' => __( 'Resource Archives', 'textdomain' ),
		'attributes' => __( 'Resource Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Resource:', 'textdomain' ),
		'all_items' => __( 'All Resources', 'textdomain' ),
		'add_new_item' => __( 'Add New Resource', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Resource', 'textdomain' ),
		'edit_item' => __( 'Edit Resource', 'textdomain' ),
		'update_item' => __( 'Update Resource', 'textdomain' ),
		'view_item' => __( 'View Resource', 'textdomain' ),
		'view_items' => __( 'View Resources', 'textdomain' ),
		'search_items' => __( 'Search Resource', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Resource', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Resource', 'textdomain' ),
		'items_list' => __( 'Resources list', 'textdomain' ),
		'items_list_navigation' => __( 'Resources list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Resources list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Resource', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-portfolio',
		'supports' => array('title', 'page-attributes', ),
		'taxonomies' => array('format', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'resource', $args );
}
add_action( 'init', 'create_resource_cpt', 0 );

function posts_per_page_resource( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'resource' ) ) {
    $query->set( 'posts_per_page', '12' );
  }
}
add_action( 'pre_get_posts', 'posts_per_page_resource' );

// Post Type Key: certificate
function create_certificate_cpt() {
	$labels = array(
		'name' => __( 'Certifications', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Certificate', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Certifications', 'textdomain' ),
		'name_admin_bar' => __( 'Certificate', 'textdomain' ),
		'archives' => __( 'Certificate Archives', 'textdomain' ),
		'attributes' => __( 'Certificate Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Certificate:', 'textdomain' ),
		'all_items' => __( 'All Certifications', 'textdomain' ),
		'add_new_item' => __( 'Add New Certificate', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Certificate', 'textdomain' ),
		'edit_item' => __( 'Edit Certificate', 'textdomain' ),
		'update_item' => __( 'Update Certificate', 'textdomain' ),
		'view_item' => __( 'View Certificate', 'textdomain' ),
		'view_items' => __( 'View Certifications', 'textdomain' ),
		'search_items' => __( 'Search Certificate', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Certificate', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Certificate', 'textdomain' ),
		'items_list' => __( 'Certifications list', 'textdomain' ),
		'items_list_navigation' => __( 'Certifications list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Certifications list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Certificate', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title', 'page-attributes', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'certificate', $args );
}
add_action( 'init', 'create_certificate_cpt', 0 );

function posts_per_page_certificate( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'certificate' ) ) {
    $query->set( 'posts_per_page', '12' );
  }
}
add_action( 'pre_get_posts', 'posts_per_page_certificate' );