<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/

function left_sidebar() {
	$args = array(
		'name'          => __( 'Left Sidebar' ),
		'id'            => 'left',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'left_sidebar' );

function right_sidebar() {
	$args = array(
		'name'          => __( 'Right Sidebar' ),
		'id'            => 'right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'right_sidebar' );

function one_footer_sidebar() {
	$args = array(
		'name'          => __( 'Footer Area One' ),
		'id'            => 'footer-one',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'one_footer_sidebar' );

function two_footer_sidebar() {
	$args = array(
		'name'          => __( 'Footer Area Two' ),
		'id'            => 'footer-2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'two_footer_sidebar' );

function three_footer_sidebar() {
	$args = array(
		'name'          => __( 'Footer Area Three' ),
		'id'            => 'footer-three',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'three_footer_sidebar' );

function four_footer_sidebar() {
	$args = array(
		'name'          => __( 'Footer Area Four' ),
		'id'            => 'footer-four',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'four_footer_sidebar' );

function five_footer_sidebar() {
	$args = array(
		'name'          => __( 'Footer Area Five' ),
		'id'            => 'footer-five',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'five_footer_sidebar' );