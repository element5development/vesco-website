<?php

/*----------------------------------------------------------------*\
	BUTTON SHOTCODE
\*----------------------------------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
		'type' => '',
		'icon' => '',
	), $atts ) );
	if ( $icon == 'phone' ) : 
		$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.'"><svg><use xlink:href="#phone" /></svg>' . do_shortcode($content) . '</a>';
	else :
		$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.'">' . do_shortcode($content) . '</a>';
	endif;
  return $link;
}
add_shortcode('btn', 'cta_button');

function chemical_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'url' => '#',
  ), $atts ) );
  $link = '<a href="'.$url.'" class="button is-chemical"><span><svg><use xlink:href="#chemical" /></svg>Chemical Emergency?</span>' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('chemical', 'chemical_button');