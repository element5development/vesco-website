<?php 
/*----------------------------------------------------------------*\

	ICONS GALLERY

\*----------------------------------------------------------------*/
?>

<section class="icons <?php if ( get_sub_field('small') ) : ?>is-small<?php endif; ?>">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<div class="icon-grid">
		<?php while ( have_rows('icons') ) : the_row(); ?>
			<a class="icon" target="_blank" href="<?php the_sub_field('website'); ?>">
				<?php $image = get_sub_field('icon'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		<?php endwhile; ?>
	</div>
</section>