<?php 
/*----------------------------------------------------------------*\

	STATS

\*----------------------------------------------------------------*/
?>

<section class="stats">
	<?php while ( have_rows('stats') ) : the_row(); ?>
		<div>
			<p><span><?php the_sub_field('stat'); ?></span><br/><?php the_sub_field('description'); ?></p>
		</div>
	<?php endwhile; ?> 
</section>