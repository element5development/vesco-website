<?php 
/*----------------------------------------------------------------*\

	STANDARD WYSIWYG

\*----------------------------------------------------------------*/
?>

<section class="wysiwyg-block">
	<div>
		<?php the_sub_field('wysiwyg'); ?>
	</div>
</section>