<?php 
/*----------------------------------------------------------------*\

	FLIP CARDS

\*----------------------------------------------------------------*/
?>

<section class="flip-cards">
	<?php while ( have_rows('flip_cards') ) : the_row(); ?>
		<div class="card">
			<div class="container">
				<?php $image = get_sub_field('image'); ?>				
				<?php if ( get_sub_field('description') ) : ?>
					<div class="front flipper" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
						<div>
							<h3><?php the_sub_field('title'); ?></h3>
							<button>Details</button>
						</div>
						<span></span>
					</div>
				<?php else : ?>
					<?php $link = get_sub_field('link'); ?>
					<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="front" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
						<div>
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php echo $link['title']; ?></p>
						</div>
						<span></span>
					</a>
				<?php endif; ?>
				<div class="back">
					<h3><?php the_sub_field('title'); ?></h3>
					<hr>
					<p><?php the_sub_field('description'); ?></p>
					<div class="buttons">
						<button>Back</button>
						<?php $link = get_sub_field('link'); ?>
						<a class="button is-red" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
</section>