<?php 
/*----------------------------------------------------------------*\

	STANDARD WYSIWYG WITH A FIXED IMAGE

\*----------------------------------------------------------------*/
?>

<a id="<?php echo the_sub_field('anchor'); ?>" class="anchor"></a>
<section class="fixed-image-wysiwyg <?php the_sub_field('image_position'); ?>">
	<div>
		<?php $image = get_sub_field('image'); ?>
		<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<div>
		<?php the_sub_field('editor'); ?>
		<div class="buttons">
			<?php while ( have_rows('buttons') ) : the_row(); ?>
				<?php $link = get_sub_field('button'); ?>
				<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endwhile; ?>
		</div>
	</div>
</section>