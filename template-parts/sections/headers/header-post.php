<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<?php if ( get_field('featured_img') ) : ?>
	<?php $image = get_field('featured_img'); ?>
<?php endif; ?>

<header class="page-title <?php if ( get_field('featured_img') ) : ?>has-image<?php endif; ?>" style="background-image: url(<?php echo $image['sizes']['xlarge']; ?>)">
	<section>

		<h1>
			<?php 
				if ( get_field('post_title') ) :
					the_field('post_title');
				else :
					the_title();
				endif;
			?>
		</h1>

	</section>
</header>