<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title is-slider">
	<?php while ( have_rows('slider') ) : the_row(); ?>
		<div class="slide">
			<div style="background-image: url('<?php the_sub_field('background'); ?>');">
				<section>
					<h1>Vesco Oil</h1>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php if ( get_sub_field('description') ) : ?>
						<p><?php the_sub_field('description'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('button') ) : ?>
						<?php $link = get_sub_field('button'); ?>
						<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>
				</section>
			</div>
		</div>
	<?php endwhile; ?>
</header>