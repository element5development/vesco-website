<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types and search results

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');
	if ( $posttype == '' || is_search() ) {
		$posttype = 'blog';
	}
	if ( is_tax('format') ) {
		$posttype = 'resource';
	}
	if ( is_search() ) {
		$posttype = '';
	}
?>

<header class="page-title has-image" style="background-image: url('<?php the_field( $posttype . '_title_bg_img', 'option'); ?>');">
	<section>

		<?php if ( is_search() ) : ?>
			<h1>Search Results for <?php echo get_search_query(); ?></h1>
		<?php else : ?>
			<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
			<?php if ( get_field( $posttype . '_title_description', 'option') ) : ?>
				<p class="subheader"><?php the_field( $posttype . '_title_description', 'option'); ?></p>
			<?php endif; ?>
		<?php endif; ?>

	</section>
</header>