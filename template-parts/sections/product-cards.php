<?php 
/*----------------------------------------------------------------*\

	PRODUCT CARDS

\*----------------------------------------------------------------*/
?>

<section class="product-cards">
	<?php while ( have_rows('products') ) : the_row(); ?>
		<div class="product">
			<div class="container">
				<div class="front">
					<?php $image = get_sub_field('image'); ?>
					<div class="image">
						<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<h3><?php the_sub_field('title'); ?></h3>
					<button>Details</button>
				</div>
				<div class="back">
					<p><?php the_sub_field('description'); ?></p>
					<button>Back</button>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
</section>