<?php 
/*----------------------------------------------------------------*\

	STANDARD CARDS

\*----------------------------------------------------------------*/
?>

<section class="standard-cards">
	<?php while ( have_rows('cards') ) : the_row(); ?>
		<div class="card">
			<?php $image = get_sub_field('image'); ?>
			<div style="background-image: url(<?php echo $image['sizes']['small'] ?>);"></div>
			<h3><?php the_sub_field('title'); ?></h3>
			<p><?php the_sub_field('description'); ?></p>
		</div>
	<?php endwhile; ?>
</section>