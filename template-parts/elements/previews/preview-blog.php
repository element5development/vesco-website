<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<?php 
	if ( get_field('featured_img') ) : 
			$image = get_field('featured_img');
			$image_url = $image['sizes']['small'];
	else : 
		$image_url = get_template_directory_uri().'/dist/images/preview-blog.png';
	endif; 
?>		

<article class="preview preview-blog">
	<div style="background-image: url(<?php echo $image_url; ?>);">
	</div>
	<div>
		<p><?php echo get_the_date( 'F j, Y' ); ?></p>
		<h3><?php the_title(); ?></h3>
		<p><?php echo get_excerpt(250); ?></p>
		<?php if ( get_field('offsite_article_url') ) : ?>
			<a class="button is-red" target="_blank" href="<?php the_field('offsite_article_url'); ?>">Continue Reading</a>
		<?php else: ?>
			<a class="button is-red" href="<?php the_permalink(); ?>">Continue Reading</a>
		<?php endif; ?>
	</div>
</article>
