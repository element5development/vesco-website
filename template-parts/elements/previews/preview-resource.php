<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-resource">
	<?php if ( has_term( 'videos', 'format') ) : ?>
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/resource_video.jpg" />
	<?php elseif ( has_term( 'pdfs', 'format') ) : ?>
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/resource_pdf.jpg" />
	<?php else : ?>
		<img src="<?php echo get_template_directory_uri(); ?>/dist/images/resource_external.jpg" />
	<?php endif; ?>
	<h4><?php the_title(); ?></h4>
	<?php $link = get_field('link'); ?>
	<a class="button is-red" target="_blank" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
</article>
