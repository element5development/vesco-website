<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<?php $image = get_field('preview_background'); ?>
<article class="preview preview-location" style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
	<div>
		<h3>
			<?php 
				if ( get_the_ID() == '340' ) :
					echo 'Columbus';
				else :
					the_field('city'); 
				endif;
			?>
		</h3>
		<p>Details & Directions</p>
	</div>
	<span></span>
	<a href="<?php the_permalink(); ?>"></a>
</article>
