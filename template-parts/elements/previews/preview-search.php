<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-search">
	<h3>
		<?php 
			if ( get_field('page_title') ) :
				the_field('page_title');
			else :
				the_title();
			endif;
		?>
	</h3>
	<p><?php echo get_excerpt(250); ?></p>
	<a class="button is-red" href="<?php the_permalink(); ?>">Continue Reading</a>
</article>