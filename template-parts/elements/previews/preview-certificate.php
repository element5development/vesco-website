<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-certificate">
	<a target="_blank" href="<?php the_field('pdf'); ?>">
		<svg>
			<use xlink:href="#pdf" />
		</svg>
		<h4><?php the_title(); ?></h4>
	</a>
</article>
